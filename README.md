
# HealthNow API Backend NodeJS EXAM

• Node.js
• MySQL



## Installation

Make sure that MySQL Server on your local is running and you have already created a database named `healthnow_users`. Please see `.env` file.

Git Clone

```bash
  git clone git@gitlab.com:jibehpz/health-now-api-exam.git
  cd health-now-api-exam
```

After cloning the repo, run `npm install` to install all node dependencies:
```bash
  npm install
```


Run migration script for the table structure:
```bash
  npm run migration
```

After the running migration script, run seeder script for the user mock data:
```bash
  npm run seed
```

Then `npm run dev` to inialize the API:
```bash
  npm run dev
```
 
## Test script (JEST + Supertest)

To initiate the API test script, just run:
```bash
  npm run test
```






    
